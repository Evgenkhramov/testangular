import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CarouselModule } from 'ngx-img-carousel'
import { HomeComponent } from './components/home/home.component';
import { TripService } from './services/trip.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TripListComponent } from './components/trip-list/trip-list.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { TokenInterceptor } from './services/token-interceptor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormModalComponent } from './components/form-modal/form-modal.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: "full" },
  { path: 'home', component: HomeComponent },
  { path: 'trip-list', component: TripListComponent },
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TripListComponent,
    FormModalComponent,

  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    NgbModule,
    CarouselModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
      ],
  providers: [AuthService,
    TripService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }],
  bootstrap: [AppComponent],
  entryComponents: [
    FormModalComponent
  ]
})
export class AppModule { }
