import { Component, OnInit } from '@angular/core';
import { ICarouselData } from 'ngx-img-carousel/public_api';
import { Router } from '@angular/router';
import { TripService } from 'src/app/services/trip.service';
import { HomePageTrip } from 'src/app/models/home-page-trip';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  constructor(private sanitizer: DomSanitizer, 
    private router: Router, private tripService: TripService) 
    {

    }

  private numberTrips: number = 4;
  private skipTrips: number =2;

  homeTrips: HomePageTrip[];

  carouselData: ICarouselData = {
    img_urls:
      [
        "../../assets/images/slider2.jpg",
        "../../assets/images/slider3.jpg",
        "../../assets/images/slider4.jpg",
      ]
  };

  ngOnInit() {
    this.tripService.getHomeTrips(this.numberTrips, this.skipTrips).then(trips => 
      {
        this.homeTrips = trips    
      });
  }

  public getSantizeUrl(url : string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  public gotoTrips() : void{
    this.router.navigate(['trip-list']);
  }
}
