import { Component, OnInit, EventEmitter, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder} from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.css']
})
export class FormModalComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(private authService: AuthService, public activeModal: NgbActiveModal, 
    private formBuilder: FormBuilder, private router: Router) 
  {
    this.createForm();
  }

  ngOnInit() {

  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', ],
      password: ['', ]
    });
  }

  get f() { return this.loginForm.controls; }

  public submitForm(){

      this.submitted = true;
      
      if (this.loginForm.invalid) {
          return;
      }
      this.activeModal.close(this.loginForm.value);
      this.loading = true;
      this.authService.login(this.f.username.value, this.f.password.value)
          .pipe(first())
          .subscribe(
            data => {
              localStorage.setItem("accessToken" , data.accessToken);
              this.router.navigate(['trip-list']);
          },
          error => {
              this.loading = false;
          });
     
    
  }

  closeModal() {
    this.activeModal.close('Modal Closed');
  }
}
