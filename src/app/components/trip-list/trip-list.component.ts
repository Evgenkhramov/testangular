import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { TripService } from 'src/app/services/trip.service';
import { Trip } from 'src/app/models/trip';

@Component({
  selector: 'app-trip-list',
  templateUrl: './trip-list.component.html',
  styleUrls: ['./trip-list.component.css']
})

export class TripListComponent implements OnInit {
  
  public show:boolean = false;
  trips: Trip[];

  constructor(private router: Router, private tripService: TripService) { }

  public gotoHome() : void{
    this.router.navigate(['home']);
  }

  ngOnInit() {
    this.tripService.getAllTrips().then(trips => 
      {
        this.trips = trips;
        this.trips.forEach(item => item.hideDetails = true)
      });
  }

  changeElementHide(element:Trip){
    element.hideDetails = !element.hideDetails;
  }
}
