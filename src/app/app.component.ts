import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormModalComponent } from './components/form-modal/form-modal.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  hideLogout:boolean = false;
  hideLogin:boolean = true;
  title = "Trip agency";
  buttonLogOut = "Log out";
  buttonText = "Login";
  constructor(private router: Router, private auth: AuthService, private modalService: NgbModal){}

  ngOnInit(){
  }

  // public gotoLogin() : void{

  //   this.router.navigate(['login']);
  //   this.hideLogin = false;
  //   this.hideLogout = true;
  // }

  public gotoLogout(): void{

    this.auth.logout();

    this.hideLogout = false;
    this.hideLogin = true;
  }

  public openFormModal() {
    const modalRef = this.modalService.open(FormModalComponent);
    
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
  }
}
