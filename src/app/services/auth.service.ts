import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loginPath: string = "http://interview.joobilix.com/api/TripAgency/Login";

  constructor(private http: HttpClient,
    private router: Router) { }

  login(username: string, password: string) {
    return this.http.post<any>(this.loginPath, JSON.stringify({ userName: username, password: password }),
      { headers: { 'Content-Type': 'application/json;' } });
  }

  logout() {
    localStorage.removeItem('accessToken');
    this.router.navigate(['home']);
  }
}
