import { Injectable } from '@angular/core';
import { Trip } from '../models/trip';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HomePageTrip } from 'src/app/models/home-page-trip';

@Injectable({
  providedIn: 'root'
})
export class TripService {

  private homeTripsUrl = 'http://interview.joobilix.com/api/TripAgency/Trips/Homepage';
  private TripsUrl = 'http://interview.joobilix.com/api/TripAgency/Trips';
  private token: string;
  constructor(private httpConnect: HttpClient){
    this.token = localStorage.getItem('accessToken');
  }
  
  getHomeTrips(numberTrips: number, skipTrips: number):Promise<HomePageTrip[]>
  {   
    const params = new HttpParams()
    .set('numberOfTrips', numberTrips.toString())
    .set('skip', skipTrips.toString());

    return this.httpConnect.get<HomePageTrip[]>(this.homeTripsUrl,{params}).toPromise().then(response => response);;
  }

  getAllTrips():Promise<Trip[]>
  {   
    return this.httpConnect.get<Trip[]>(this.TripsUrl).toPromise().then(response => response);
  }
}
