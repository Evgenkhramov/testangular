export class HomePageTrip {
    public title: string;
    public id: string;
    public imageUrl: string;
    public location: string;
    public price: Number;
    public fromDate: Date;
    public toDate: Date;   
}
