export class Trip {
    public title: string;
    public id: string;
    public imageUrl:   string;
    public location: string;
    public price: Number;
    public fromDate: Date;
    public toDate: Date;
    public description: string;
    public avgRating: Number;
    public like: boolean;
    public hideDetails: boolean = true;
}
