export class LoginResponse {
    public accessToken: string;
    public expires: number;
}
